# jgs-container-image-utils

Helper programs for handling container images

**load_image.py**
Load a container image from a local archive only if the image digest
differs from that of an already loaded image with a specified name.
